Run "make" to compile, "make clean" to remove the .o files, or "make mrproper" to remove the .o files and the executable.

See the comments in the codes for explanations of how it works.
